# Introduction

bphon-releases is the landing page for the software releases of the BPHON program

# Description

BPHON is a plugin for [ChimeraX](https://www.rbvi.ucsf.edu/chimerax/)
that allows one to take molecular structures and synthetically generate
a nmrPipe FT2 file that predicts NMR peak locations.

BPHON was designed by the team at NMRFAM at the University of Wisconsin
at Madison. Development began in 2023.

# Download

- Download a release package and manual here:

  Latest version is at this link: https://git.doit.wisc.edu/nmrfam-public/bphon-releases/-/tree/main/release-files/1.0.1/

  All versions are present here: https://git.doit.wisc.edu/nmrfam-public/bphon-releases/-/tree/main/release-files/

# Installation guide

  - Install ChimeraX on your computer

      Visit this link: https://www.cgl.ucsf.edu/chimerax/download.html

      Download a version greater than or equal to 1.7

      Install it using instructions at that site

  - Install nmrPipe on your computer
  
      See https://www.ibbr.umd.edu/nmrpipe/install.html

  - Install sparky on your computer

      Follow the Download arrow at https://nmrfam.wisc.edu/nmrfam-sparky-distribution/
 
  - Install ShiftX2 on your computer

      Visit this link:
        
        https://weberdak.github.io/traj2nmr/html/source/installation.html#shiftx2

      Read the instructions on how to install ShiftX2

      Install it (and make sure that in your profile you set the env vars that are outlined in the instructions)

      DO NOT INSTALL ANYTHING ELSE MENTIONED IN THOSE INSTRUCTIONS

      For instance stop following the install instructions when
      the discussion gets to SPARTA+

  - Make sure the ShiftX2 code is accessible in the path:
  
      In the ShiftX2 install instructions you visited there is discussion
      in place to put these lines of code:
      
        export SHIFTX2_DIR=~/Programs/shiftx2-linux
        export PATH=$PATH:$SHIFTX2_DIR

      in your ~/.bashrc file or in your ~/.profile file.
      
      However that assumes you will be running ShiftX2 via the Bash shell.
      But nmrPipe uses the C-shell. So you should also make the following
      equivalent changes to your ~/.cshrc file:
      
        setenv SHIFTX2_DIR ~/Programs/shiftx2-linux
        setenv PATH ${PATH}:${SHIFTX2_DIR}

      Please note that in both cases if you installed shiftx2-linux in
      some place other than ~/Programs then you should make the same path
      change to these file edit examples above.
      
  - Install BPHON in ChimeraX

      Start up ChimeraX from a csh or tcsh terminal shell. This is necessary
      so that nmrPipe has its correct operating environment. Otherwise BPHON
      will not be able to run simulations and you will get weird error reports.
      (It is as simple as launching a bash terminal and then in that shell
      enter "csh" or "tcsh").

      In ChimeraX's command window type "toolshed install <path-to-bphon.whl-file>"

      It will go into the Tools :: Structure Analysis menu

      Once BPHON has been installed try running it via the Tools :: Structure Analysis menu entry
      
        The first time you launch BPHON you will get an error dialog related to the
        "traj2nmr" package not being found.

        Just close the dialog and restart chimerax (this should only ever need to be done once)

        Once again launch BPHON via the Tools :: Structure Analysis menu entry

        A UI will be embedded on the lower right pane of ChimeraX and BPHON is ready
           for use.

# Uninstall guide

  If you ever need to uninstall BPHON from chimerax enter this in the chimerax command line:

    toolshed uninstall BphonTool

  To make sure chimerax cleans up well, exit and restart chimerax

# LICENSE

  Copyright UW-Madison Board of Regents (2023-present). All rights reserved.

  The Regents of the University of Wisconsin-Madison hereby grant the end user an internal, non-exclusive, non-commercial license to NMRFAM software for academic and scholarly purposes only. All rights reserved to the Regents of the University of Wisconsin-Madison. The end user agrees and acknowledges that redistribution to third parties not covered by this license is explicitly prohibited.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  For non-academic use, please contact Justin Anderson at the Wisconsin Alumni Research Foundation (janderson@warf.org) to obtain a commercial license.
  
# How to cite

  TODO Once publication is accepted, insert DOI, link and citation here

# DOI of software version(s)

  TODO : Zenodo?
